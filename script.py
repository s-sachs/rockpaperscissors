import random
import json

languages = ["english", "german", "french"]
translations = None
chosenLanguage = 0

choicesMap = {
    "rock": ["scissors"],
    "paper": ["rock", "well"],
    "scissors": ["paper"],
    "well": ["rock", "scissors"]
}
choices = list(choicesMap.keys())

playerName = None
playerChoice = None
computerChoice = None
playerWins = 0
computerWins = 0
ties = 0


def loadTranslations():
    global translations
    with open('translations.json') as translationFile:
        translations = json.load(translationFile)
        

def translate(key):
    return translations.get(key)[chosenLanguage]


def chooseLanguage():
    # Print language choices
    print("Choose one of the following languages: (enter the number of the language)")
    for index in range(len(languages)):
        print(f"{index + 1}. {languages[index]}")

    while True:
        # Try to parse input into an integer
        try:
            userInput = int(input()) - 1
        except ValueError:
            print("This is not a valid number")
            continue

        # Catch invalid numbers
        if userInput < 0 or len(languages) <= userInput:
            print("This is not a valid choice")
            continue

        # Stop the while loop, as the input is a valid number
        break

    return userInput

# Prompts the user to enter a name
def enterName():
    print(translate("enterName"))
    result = input()
    print(translate("hello") % result)
    return result


# Prompts the user to pick a choice
def pickChoice():
    print(translate("pickChoice"))
    
    for index in range(len(choices)):
        # print(f"{index + 1}. {choices[index]}")
        print("%s. %s" % (index + 1, translate(choices[index]))) 

    while True:
        # Try to parse input into an integer
        try:
            userInput = int(input()) - 1
        except ValueError:
            print(translate("numberNotValid"))
            continue
        
        # Catch invalid numbers
        if userInput < 0 or len(choices) <= userInput:
            print(translate("choiceNotValid"))
            continue

        break

    print(translate("playerPick") % translate(choices[userInput]))
    return userInput
    

# Pick random action
def pickComputerChoice():
    result = random.randint(0, len(choices) - 1)
    print(translate("computerPick") % translate(choices[result]))
    return result



# compare
def determineWinnner(player, computer):
    global ties, playerWins, computerWins

    # If player and computer are equal
    if (player == computer):
        print(translate("tie"))
        ties = ties + 1
        return 0
        
    # If player beats computer
    if (computer in choicesMap.get(player)):
        print(translate("playerWon"))
        playerWins = playerWins + 1
        return 1
    
    # If computer beats player
    print(translate("computerWon") % playerName)
    computerWins = computerWins + 1
    return -1


loadTranslations()
# PLAY
chosenLanguage = chooseLanguage()
print("------------")
playerName = enterName()

while True:
    print("------------")
    playerChoice = pickChoice()
    print("------------")
    computerChoice = pickComputerChoice()
    print("------------")
    determineWinnner(choices[playerChoice], choices[computerChoice])

    # Display the score
    print("------------")
    print(f"{playerName} {playerWins} – {computerWins} computer | {ties} ties")
    print("------------")
    

    userInput = input(translate("playAgain") + " ")
    if (userInput.lower() == "y"):
        continue

    break
    

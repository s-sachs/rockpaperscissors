import json

translations = {
    "enterName": ["Enter your name:", "Gib deinen Namen ein:", "Entre ton nom:"],
    "hello": ["Hello %s!", "Hallo %s!", "Salut %s !"],
    "pickChoice": ["What do you choose?", "Was wählst du aus?", "Qu'est-ce que tu choisis ?"],
    "playerPick": ["You chose %s!", "Du hast %s ausgewählt!", "Tu as choisi %s !"],
    "numberNotValid": ["This is not a valid number!", "Das ist keine gültige Nummer!", "Ce n'est pas un nombre valide !"],
    "choiceNotValid": ["This is not a valid choice!", "Das ist keine gültige Auswahl!", "Ce n'est pas un choix valide !"],
    "computerPick": ["The opponent picked %s!", "Der Gegner hat %s ausgewählt!", "L'adversaire a choisi %s "],
    "playerWon": ["You won!", "Du hast gewonnen!", "Tu as gagné !"],
    "computerWon": ["%s, you lost !", "%s, du hast verloren!", "%s, tu as perdu !"],
    "tie": ["Tie!", "Unentschieden!", "Égalité !"],
    "playAgain": ["Play again? (y/n)", "Nochmal spielen? (y/n)", "Rejouer ? (y/n)"],
    "rock": ["rock", "Stein", "pierre"],
    "paper": ["paper", "Papier", "feuille"],
    "scissors": ["scissors", "Schere", "ciseaux"],
    "well": ["well", "Brunnen", "puits"],
}

with open("translations.json", "w") as target:
    json.dump(translations, target)